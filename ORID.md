**O (Objective): What did we learn today? What activities did you do? What scenes have impressed you?**

1. conde review：我昨天的作业存在冗余代码，还有magic variable ，在后续作业中需要不断改进
2. spring boot 集成测试：实践了如何测试controller，并实践了相关api如mockMvc等
3. 学习了spring boot service层的测试：一开始我想的是像验证controller一样验证，我们模拟的Repository也是可以进行实际的操作的，后来在老师的引导下慢慢理解了service层测试需要我们做的事情

**R (Reflective): Please use one word to express your feelings about today's class.**

理解困难

**I (Interpretive): What do you think about this? What was the most meaningful aspect of this activity?**

我在今天下午的service层的测试卡了很久，也在老师的帮助下理解了service层测试，举一反三，我也优点理解了其他层的测试应该如何展开。

**D (Decisional): Where do you most want to apply what you have learned today? What changes will you make?**

学习测试相关的api

