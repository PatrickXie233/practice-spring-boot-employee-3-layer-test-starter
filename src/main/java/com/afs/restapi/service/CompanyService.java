package com.afs.restapi.service;

import com.afs.restapi.model.Company;
import com.afs.restapi.repository.CompanyRepository;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class CompanyService {

    private final CompanyRepository companyRepository
            ;

    public CompanyService(CompanyRepository companyRepository) {
        this.companyRepository = companyRepository;
    }

    public List<Company> getCompanies() {
        return companyRepository.getCompanies();
    }

    public Company addCompany(Company company) {
        return companyRepository.addCompany(company);
    }
}