package com.afs.restapi.service;

import com.afs.restapi.exception.AgeNoMatchSalaryException;
import com.afs.restapi.exception.AgeOutOfRangeException;
import com.afs.restapi.exception.EmployeeHaveDepart;
import com.afs.restapi.model.Employee;
import com.afs.restapi.repository.EmployeeRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmployeeService {

    private EmployeeRepository employeeRepository;


    public EmployeeService(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    public List<Employee> getAll() {
        return employeeRepository.findAll();
    }

    public Employee findById(int id) {
        return employeeRepository.findById(id);
    }

    public List<Employee> findByGender(String gender) {
        return employeeRepository.findByGender(gender);
    }

    public List<Employee> findByPage(int pageNumber, int pageSize) {
        return employeeRepository.findByPage(pageNumber, pageSize);
    }

    public Employee insert(Employee employee) {
        if (employee.getAge() < 16 || employee.getAge() > 65) {
            throw new AgeOutOfRangeException();
        }
        if (employee.getAge() >= 30 && employee.getSalary() < 20000) {
            throw new AgeNoMatchSalaryException();
        }
        employee.setStatus(true);
        return employeeRepository.insert(employee);
    }

    public Employee update(int id, Employee employee) {
        if (!employee.getStatus()){
            throw new EmployeeHaveDepart();
        }
        return employeeRepository.update(id, employee);
    }

    public void delete(int id) {
        employeeRepository.delete(id);
    }
}