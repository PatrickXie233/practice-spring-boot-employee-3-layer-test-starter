package com.afs.restapi;

import com.afs.restapi.exception.AgeNoMatchSalaryException;
import com.afs.restapi.exception.AgeOutOfRangeException;
import com.afs.restapi.exception.EmployeeHaveDepart;
import com.afs.restapi.model.Employee;
import com.afs.restapi.repository.EmployeeRepository;
import com.afs.restapi.service.EmployeeService;
import org.assertj.core.error.AssertionErrorMessagesAggregator;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;


public class EmployeeServiceTest {
    EmployeeRepository employeeRepository = mock(EmployeeRepository.class);
    EmployeeService employeeService = new EmployeeService(employeeRepository);

    @Test
    void should_throw_AgeOutOfRangeException_when_insert_Employee_given_employee_age15_and_age65() {
//        given
        Employee employeeYounger = new Employee(1, "bob", 15, "male", 10000);
        Employee employeeOlder = new Employee(2, "lisa", 70, "female", 20000);
//        when then
        assertThrows(AgeOutOfRangeException.class, () -> employeeService.insert(employeeYounger));
        assertThrows(AgeOutOfRangeException.class, () -> employeeService.insert(employeeOlder));

        verify(employeeRepository, times(0)).insert(employeeYounger);
        verify(employeeRepository, times(0)).insert(employeeOlder);
    }


    @Test
    void should_throw_AgeNoMatchSalaryException_when_insert_employee_given_employee_age30() {
        Employee noMatchSalaryEmployee = new Employee(1, "bob", 31, "male", 10000);

        assertThrows(AgeNoMatchSalaryException.class, () -> employeeService.insert(noMatchSalaryEmployee));

        verify(employeeRepository, times(0)).insert(noMatchSalaryEmployee);
    }

    @Test
    void should_set_true_status_when_insert_employee_given_true_employee() {
//        given
        Employee trueEmployee = new Employee(1, "bob", 23, "male", 10000);
        Employee shouldReturnEmployee = new Employee(1, "bob", 23, "male", 10000);
        shouldReturnEmployee.setStatus(true);
        when(employeeRepository.insert(any())).thenReturn(shouldReturnEmployee);
//        when
        Employee employeeToSaved = employeeService.insert(trueEmployee);
//        then
        assertTrue(employeeToSaved.isStatus());
        verify(employeeRepository).insert(argThat(employeeToSave -> {
            assertTrue(employeeToSave.isStatus());
            return true;
        }));
    }

    @Test
    void should_excute_employeeRepository_when_delete_employee_given_employee_id() {
        employeeService.delete(1);
        verify(employeeRepository).delete(Mockito.eq(1));
    }

    @Test
    void should_return_employee_when_update_employee_given_employee() {
        Employee employeePre = new Employee(1, "bob", 24, "male", 10000);
        Employee employeeNow = new Employee(1, "bob", 23, "male", 10000);
        employeeRepository.insert(employeePre);
        when(employeeRepository.update(1,employeeNow)).thenReturn(employeeNow);
        employeeService.update(1,employeeNow);
        verify(employeeRepository,times(1)).update(Mockito.eq(1),Mockito.any());
    }

    @Test
    void should_throw_EmployeeHaveDepart_when_update_given_employee() {
//        give
        Employee employeePre = new Employee(1, "bob", 25, "male", 10000);
        employeeRepository.insert(employeePre);
        employeeRepository.delete(1);
        Employee employeeNow = new Employee(1, "bob", 24, "male", 10000);
        employeeNow.setStatus(false);
        assertThrows(EmployeeHaveDepart.class,()->employeeService.update(1,employeeNow));
    }

}
