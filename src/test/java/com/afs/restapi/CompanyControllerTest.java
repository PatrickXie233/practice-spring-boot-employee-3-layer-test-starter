package com.afs.restapi;

import com.afs.restapi.exception.NotFoundException;
import com.afs.restapi.model.Company;
import com.afs.restapi.model.Employee;
import com.afs.restapi.repository.CompanyRepository;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class CompanyControllerTest {
    @Autowired
    MockMvc mockMvc;
    ObjectMapper mapper = new ObjectMapper();
    @Autowired
    CompanyRepository companyRepository;

    @BeforeEach
    void setUp() {
        companyRepository.clearAll();
    }

    @Test
    void should_get_all_company_when_perform_get_given_companies() throws Exception {
        //given

        Company company = getCompanyOOCL();
        companyRepository.addCompany(company);

        //when
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/companies"))
                .andReturn();

        //then
        MockHttpServletResponse response = mvcResult.getResponse();
        assertEquals(200, response.getStatus());
        List<Company> companies = mapper.readValue(response.getContentAsString(), new TypeReference<List<Company>>() {
        });
        assertEquals(1, companies.size());
        Company resultCompany = companies.get(0);
        assertEquals(1, resultCompany.getId());
        assertEquals("oocl", resultCompany.getCompanyName());
    }

    private static Company getCompanyOOCL() {
        List<Employee> employees = new ArrayList<>();
        employees.add(new Employee(1, "Lily1", 20, "Female", 8000));
        return new Company(1, "oocl", employees);
    }

    private static Company getCompanyOOCLL() {
        List<Employee> employees = new ArrayList<>();
        employees.add(new Employee(2, "zhangsan", 30, "male", 30000));
        return new Company(1, "oocll", employees);
    }

    private static Company getCompanyCargoSmart() {
        List<Employee> employees = new ArrayList<>();
        employees.add(new Employee(3, "lisi", 40, "male", 33000));
        return new Company(1, "CargoSmart", employees);
    }

    @Test
    void should_get_all_companies_when_perform_get_given_companies_use_matchers() throws Exception {
        //given
        Company company = getCompanyOOCL();
        companyRepository.addCompany(company);

        //when
        mockMvc.perform(MockMvcRequestBuilders.get("/companies"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].id").isNumber())
                .andExpect(jsonPath("$[0].id").value(1))
                .andExpect(jsonPath("$[0].companyName").isString())
                .andExpect(jsonPath("$[0].companyName").value("oocl"))
                .andExpect(jsonPath("$[0].employees").isArray())
                .andExpect(jsonPath("$[0].employees[0].id").value(1))
                .andExpect(jsonPath("$[0].employees[0].name").value("Lily1"))
                .andExpect(jsonPath("$[0].employees[0].name").isString())
                .andExpect(jsonPath("$[0].employees[0].gender").value("Female"))
                .andExpect(jsonPath("$[0].employees[0].gender").isString())
                .andExpect(jsonPath("$[0].employees[0].salary").value(8000))
                .andExpect(jsonPath("$[0].employees[0].salary").isNumber());
    }

    @Test
    void should_return_companies_oocl_with_id_1_when_perform_get_by_id_given_companies_in_repo() throws Exception {
        //given
        Company companyOOCL = getCompanyOOCL();
        Company companyOOCLL = getCompanyOOCLL();
        companyRepository.addCompany(companyOOCL);
        companyRepository.addCompany(companyOOCLL);
        //when
        mockMvc.perform(MockMvcRequestBuilders.get("/companies/1"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(jsonPath("$.id").isNumber())
                .andExpect(jsonPath("$.id").value(1))
                .andExpect(jsonPath("$.companyName").isString())
                .andExpect(jsonPath("$.companyName").value("oocl"))
                .andExpect(jsonPath("$.employees").isArray())
                .andExpect(jsonPath("$.employees[0].id").value(1))
                .andExpect(jsonPath("$.employees[0].name").value("Lily1"))
                .andExpect(jsonPath("$.employees[0].name").isString())
                .andExpect(jsonPath("$.employees[0].gender").value("Female"))
                .andExpect(jsonPath("$.employees[0].gender").isString())
                .andExpect(jsonPath("$.employees[0].salary").value(8000))
                .andExpect(jsonPath("$.employees[0].salary").isNumber());
    }

    @Test
    void should_return_one_page_companies_when_perform_get_by_page_given_companies_in_repo() throws Exception {

        //given
        Company companyOOCL = getCompanyOOCL();
        Company companyOOCLL = getCompanyOOCLL();
        Company companyCargoSmart = getCompanyCargoSmart();

        companyRepository.addCompany(companyOOCL);
        companyRepository.addCompany(companyOOCLL);
        companyRepository.addCompany(companyCargoSmart);

        //when
        mockMvc.perform(MockMvcRequestBuilders.get("/companies?pageIndex=1&pageSize=2"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].id").isNumber())
                .andExpect(jsonPath("$[0].id").value(1))
                .andExpect(jsonPath("$[0].companyName").isString())
                .andExpect(jsonPath("$[0].companyName").value("oocl"))
                .andExpect(jsonPath("$[0].employees").isArray())
                .andExpect(jsonPath("$[0].employees[0].id").value(1))
                .andExpect(jsonPath("$[0].employees[0].name").value("Lily1"))
                .andExpect(jsonPath("$[0].employees[0].name").isString())
                .andExpect(jsonPath("$[0].employees[0].age").value(20))
                .andExpect(jsonPath("$[0].employees[0].age").isNumber())
                .andExpect(jsonPath("$[0].employees[0].gender").value("Female"))
                .andExpect(jsonPath("$[0].employees[0].gender").isString())
                .andExpect(jsonPath("$[0].employees[0].salary").value(8000))
                .andExpect(jsonPath("$[0].employees[0].salary").isNumber())
                .andExpect(jsonPath("$[1].id").isNumber())
                .andExpect(jsonPath("$[1].id").value(2))
                .andExpect(jsonPath("$[1].companyName").isString())
                .andExpect(jsonPath("$[1].companyName").value("oocll"))
                .andExpect(jsonPath("$[1].employees").isArray())
                .andExpect(jsonPath("$[1].employees[0].id").value(2))
                .andExpect(jsonPath("$[1].employees[0].name").value("zhangsan"))
                .andExpect(jsonPath("$[1].employees[0].name").isString())
                .andExpect(jsonPath("$[1].employees[0].age").value(30))
                .andExpect(jsonPath("$[1].employees[0].age").isNumber())
                .andExpect(jsonPath("$[1].employees[0].gender").value("male"))
                .andExpect(jsonPath("$[1].employees[0].gender").isString())
                .andExpect(jsonPath("$[1].employees[0].salary").value(30000))
                .andExpect(jsonPath("$[1].employees[0].salary").isNumber());
    }
    @Test
    void should_return_companies_save_with_id_when_perform_post_given_a_companies() throws Exception {
        //given
        Company company = getCompanyOOCL();
        String companyJson = mapper.writeValueAsString(company);

        //when
        mockMvc.perform(MockMvcRequestBuilders.post("/companies")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(companyJson))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.id").isNumber())
                .andExpect(jsonPath("$.id").value(1))
                .andExpect(jsonPath("$.companyName").isString())
                .andExpect(jsonPath("$.companyName").value("oocl"))
                .andExpect(jsonPath("$.employees").isArray())
                .andExpect(jsonPath("$.employees[0].id").value(1))
                .andExpect(jsonPath("$.employees[0].name").value("Lily1"))
                .andExpect(jsonPath("$.employees[0].name").isString())
                .andExpect(jsonPath("$.employees[0].gender").value("Female"))
                .andExpect(jsonPath("$.employees[0].gender").isString())
                .andExpect(jsonPath("$.employees[0].salary").value(8000))
                .andExpect(jsonPath("$.employees[0].salary").isNumber());
        Company companySaved = companyRepository.findById(1);
        assertEquals(company.getId(), companySaved.getId());
        assertEquals(company.getCompanyName(), companySaved.getCompanyName());
        assertEquals(company.getEmployees().get(0).getId(), companySaved.getEmployees().get(0).getId());
    }
    @Test
    void should_update_companies_in_repo_when_perform_put_by_id_given_companies_in_repo_and_update_info() throws Exception {

        //given
        Company companyOOCL = getCompanyOOCL();
        companyRepository.addCompany(companyOOCL);
        Company updatedCompanyOOCL = getCompanyOOCL();
        updatedCompanyOOCL.setCompanyName("东方海外");
        String susanJson = mapper.writeValueAsString(updatedCompanyOOCL);

        //when
        mockMvc.perform(MockMvcRequestBuilders.put("/companies/{id}", 1)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(susanJson))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").isNumber())
                .andExpect(jsonPath("$.id").value(1))
                .andExpect(jsonPath("$.companyName").isString())
                .andExpect(jsonPath("$.companyName").value("东方海外"))
                .andExpect(jsonPath("$.employees").isArray())
                .andExpect(jsonPath("$.employees[0].id").value(1))
                .andExpect(jsonPath("$.employees[0].name").value("Lily1"))
                .andExpect(jsonPath("$.employees[0].name").isString())
                .andExpect(jsonPath("$.employees[0].gender").value("Female"))
                .andExpect(jsonPath("$.employees[0].gender").isString())
                .andExpect(jsonPath("$.employees[0].salary").value(8000))
                .andExpect(jsonPath("$.employees[0].salary").isNumber());
        Company companyRepo = companyRepository.findById(1);
        assertEquals(updatedCompanyOOCL.getCompanyName(), companyRepo.getCompanyName());
    }
    @Test
    void should_del_companies_in_repo_when_perform_del_by_id_given_companies() throws Exception {
        //given
        Company company = getCompanyOOCL();
        companyRepository.addCompany(company);

        //when
        mockMvc.perform(MockMvcRequestBuilders.delete("/companies/{id}", 1))
                .andExpect(status().isNoContent());

        //then
        assertThrows(NotFoundException.class, () -> companyRepository.findById(1));
    }
}
